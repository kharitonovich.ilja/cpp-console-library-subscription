#ifndef precipitation_H
#define precipitation_H

#include "constants.h"
using namespace std;

struct date
{
    int day;
    int month;
};

struct precipitation_amount
{
    double amount;
};

struct precipitation_type
{
    char type[MAX_STRING_SIZE];
};

struct precipitation
{
    date date;
    precipitation_amount precipitation_amount;
    precipitation_type precipitation_type;
};
#endif